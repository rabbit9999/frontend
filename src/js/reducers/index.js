import {
    ADD_EMAIL_TO_QUEUE,
    MISSING_SUBJECT,
    MISSING_MESSAGE,
    MISSING_ADDRESS,
    API_SUCCESS,
    API_ERROR
} from "../constants/action-types";

const initialState = {
    emailAddress: '',
    emailSubject: '',
    emailMessage: '',
    result: ''
};

function rootReducer(state = initialState, action) {
    if (action.type === ADD_EMAIL_TO_QUEUE) {
        return Object.assign({}, state, {
            emailAddress: action.emailAddress,
            emailSubject: action.emailSubject,
            emailMessage: action.emailMessage,
        });
    }

    if (action.type === MISSING_ADDRESS) {
        return Object.assign({}, state, {
            result: 'Address is required'
        });
    }


    if (action.type === MISSING_SUBJECT) {
        return Object.assign({}, state, {
            result: 'Subject is required'
        });
    }

    if (action.type === MISSING_MESSAGE) {
        return Object.assign({}, state, {
            result: 'Message is required'
        });
    }

    if (action.type === API_SUCCESS) {
        let payload = action.payload;
        let resultMessage = '';
        if(payload.hasError){
            resultMessage += 'ERROR: ';
        }

        resultMessage += payload.message;

        return Object.assign({}, state, {
            result: resultMessage
        });
    }

    if (action.type === API_ERROR) {
        return Object.assign({}, state, {
            result: 'Rest API Critical error occurred'
        });
    }

    return state;
}

export default rootReducer;
