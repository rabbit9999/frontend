import { ADD_EMAIL_TO_QUEUE, API_ERROR, API_SUCCESS } from "../constants/action-types";
import {takeEvery, call, put} from "redux-saga/effects";
import configData from "../../config.json";

export default function* watcherSaga() {
    yield takeEvery(ADD_EMAIL_TO_QUEUE, workerSaga);
}

function* workerSaga(action) {
    try {
        const payload = yield call(getData, action.payload);
        yield put({type: API_SUCCESS, payload});
    } catch (e) {
        yield put({type: API_ERROR, payload: e});
    }
}

function getData(payload) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            address: payload.emailAddress,
            subject: payload.emailSubject,
            message: payload.emailMessage
        })
    };

    let apiUrl = configData.serverUrl + configData.addEmail;

    return fetch(apiUrl, requestOptions)
        .then(response => response.json());
}
