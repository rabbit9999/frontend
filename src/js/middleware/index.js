import { ADD_EMAIL_TO_QUEUE, MISSING_ADDRESS, MISSING_MESSAGE, MISSING_SUBJECT} from "../constants/action-types";

export function checkFormMiddleware({ dispatch }) {
  return function(next) {
    return function(action) {
      if (action.type === ADD_EMAIL_TO_QUEUE) {

        if(!action.payload.emailAddress) {
          return dispatch({ type: MISSING_ADDRESS });
        }

        if(!action.payload.emailSubject) {
          return dispatch({ type: MISSING_SUBJECT });
        }

        if(!action.payload.emailMessage) {
          return dispatch({ type: MISSING_MESSAGE });
        }
      }
      return next(action);
    };
  };
}
