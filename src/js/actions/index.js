import {ADD_EMAIL_TO_QUEUE} from "../constants/action-types";

export function addEmailToQueue(payload) {
  return { type: ADD_EMAIL_TO_QUEUE, payload };
}

export function getData() {
  return { type: "DATA_REQUESTED" };
}
