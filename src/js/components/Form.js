import React, { Component } from "react";
import { connect } from "react-redux";
import "./styles.css"

import { addEmailToQueue } from "../actions/index";

function mapDispatchToProps(dispatch) {
  return {
    addEmailToQueue: (emailAddress, emailSubject, emailMessage) => dispatch(addEmailToQueue(emailAddress, emailSubject, emailMessage))
  };
}

class ConnectedForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailAddress: "",
      emailSubject: "",
      emailMessage: "",
      result: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { emailAddress , emailSubject, emailMessage } = this.state;
    this.props.addEmailToQueue({emailAddress, emailSubject, emailMessage});
  }
  render() {
    const { emailAddress , emailSubject, emailMessage } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label className="field_label" htmlFor="emailAddress">E-Mail address: </label>
          <input
            type="email"
            id="emailAddress"
            className="input_field"
            value={emailAddress}
            onChange={this.handleChange}
          />
        </div>
        <div>
          <label className="field_label" htmlFor="emailSubject">Subject: </label>
          <input
              type="text"
              id="emailSubject"
              className="input_field"
              value={emailSubject}
              onChange={this.handleChange}
          />
        </div>
        <div>
          <label className="field_label" htmlFor="emailMessage">Subject: </label>
          <textarea
              id="emailMessage"
              className="input_field message_field"
              onChange={this.handleChange}
              value={emailMessage}
          />
        </div>
        <button type="submit">Add message to queue</button>
      </form>
    );
  }
}

const Form = connect(
  null,
  mapDispatchToProps
)(ConnectedForm);

export default Form;
