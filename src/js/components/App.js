import React from "react";
import Result from "./Result.js";
import Form from "./Form.js";
import "./styles.css";

const App = () => (
  <div className="App">
    <div>
      <Result />
    </div>
    <div>
      <h2>Add email to queue</h2>
      <Form />
    </div>
  </div>
);

export default App;
