import React from "react";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return { result: state.result};
};

const Res = ({ result}) => (
    <div>{result}</div>
);

const Result = connect(mapStateToProps)(Res);

export default Result;
